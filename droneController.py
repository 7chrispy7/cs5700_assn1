from UI import UserInterface

def main():
    userInterface = UserInterface()
    userInterface.initDrone()
    userInterface.mainLoop()

main()
