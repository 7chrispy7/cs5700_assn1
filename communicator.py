import socket
import time

class Communicator:
    __socket = None
    __UDP_IP = None
    __UDP_PORT = None

    def __init__(self, UDP_IP, UDP_PORT):
        self.__socket = socket.socket(socket.AF_INET,socket.SOCK_DGRAM)
        self.__socket.settimeout(1.0)
        self.__UDP_IP = UDP_IP
        self.__UDP_PORT = UDP_PORT
    def sendMessage(self, message):
        message=message.encode('UTF-8')
        self.__socket.sendto(message,(self.__UDP_IP,self.__UDP_PORT))
        retries = 0
        while(retries < 3):
            try:
                received = self.__socket.recv(64)
                if received.decode() == 'ok':
                    retries = 3
                else:
                    print(str(received))
            except:
                print('retry ' + str(retries+1) + ' message: ' + message.decode(),flush=True)
                retries += 1
        time.sleep(3)
