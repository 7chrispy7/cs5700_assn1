from drone import Drone

class UserInterface:
    drone = None
    def initDrone(self):
        print("Welcome to the drone controller!")
        print("Please note default values will use the simulator rather than a drone.\n")
        UDP_IP = input("Please enter an IP or [d] for default: ")
        UDP_PORT = input("Please enter a port or [d] for default: ")
        if(UDP_IP == 'd'):
            UDP_IP = '127.0.0.1'
        if(UDP_PORT == 'd'):
            UDP_PORT = 8889
        self.drone = Drone(UDP_IP,UDP_PORT)
    def mission1(self):
        self.drone.ping()
        self.drone.launch()
        self.drone.fly('l',40)
        self.drone.fly('r',40)
        self.drone.fly('f',40)
        self.drone.fly('b',40)
        self.drone.fly('u',40)
        self.drone.fly('d',40)
        self.drone.land()
    def mission2(self):
        self.drone.ping()
        self.drone.launch()
        self.drone.flip('l')
        self.drone.flip('r')
        self.drone.flip('f')
        self.drone.flip('b')
        self.drone.land()
    def mission3(self):
        self.drone.ping()
        self.drone.launch()
        self.drone.rotate('r', 90)
        self.drone.fly('f', 400)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 400)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 800)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 800)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 800)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 400)
        self.drone.rotate('l', 90)
        self.drone.fly('f', 400)
        self.drone.rotate('r', 90)
        self.drone.land()
    def runMission(self,mission):
        if mission == '1':
            self.mission1()
        elif mission == '2':
            self.mission2()
        elif mission == '3':
            self.mission3()
        else:
            print('That is not a valid mission.  Please choose a mission [1-3].')
    def mainLoop(self):
        isDone = False
        while not isDone:
            print('Please select a mission [1-3].\nOr type [q] to quit.')
            mission = input()
            if mission != 'q':
                self.runMission(mission)
            else:
                isDone = True
