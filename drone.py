import time
from communicator import Communicator

class Drone:
    communicator=None

    def __init__(self, UDP_IP, UDP_PORT):
        self.communicator=Communicator(UDP_IP, UDP_PORT)
    def ping(self):
        self.communicator.sendMessage('command')
    def launch(self):
        self.communicator.sendMessage('takeoff')
    def land(self):
        self.communicator.sendMessage('land')
    def fly(self, direction, distance):
        directions = {
            'u': "up",
            'd': "down",
            'l': "left",
            'r': "right",
            'f': "forward",
            'b': "back"
        }
        self.communicator.sendMessage(directions.get(direction) + ' ' + str(distance))
    def flip(self, direction):
        self.communicator.sendMessage('flip ' + direction)
    def rotate(self, direction, degrees):
        directions = {
            'l': "ccw",
            'r': "cw"
        }
        self.communicator.sendMessage(directions.get(direction) + ' ' + str(degrees))
